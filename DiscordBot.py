import asyncio
import datetime
import queue
import threading

import sqlite3
from typing import List, Dict

import discord
from discord import Message

from IntentsDB import IntentsDB
from NLTKModel import NLTKModel
from OpenAIModel import OpenAIModel

intents = discord.Intents.default()
intents.members = True
intents.message_content = True

BOT_ID = "898251015789371412"
BOT_PERMISSION_NUMBER = "397485927488"
BOT_INVITE_LINK = f'https://discord.com/oauth2/authorize?client_id={BOT_ID}&scope=bot&permissions={BOT_PERMISSION_NUMBER}'


def clean_content(msg: str) -> str:
    # '<@898251015789371412> salut' devient -> 'salut' par exemple
    return msg.replace(f'<@{BOT_ID}>', '').strip()


class DiscordBot(discord.Client):
    def __init__(self, intents_db: IntentsDB, nltk_model: NLTKModel):
        super().__init__(intents=intents)
        self.intents_db = intents_db
        self.nltk_model = nltk_model
        self.wrong_intent = "✅"

        self.training_lock = threading.Lock()
        self.training_queue = queue.Queue(maxsize=2)
        self.current_training_thread = None

    async def on_ready(self):
        print('Mirai Bot NLP is ready')

    async def on_message(self, message: Message):
        if message.author == self.user:
            return

        print('received message: ' + message.content)

        if message.content.startswith('!'):
            command = message.content[1:]
            await self.handle_command(command, message)
        else:
            classified_intent = self.nltk_model.detect_intent(message.content)
            print('intent of message is ' + str(classified_intent), end='\n\n')
            if classified_intent and classified_intent != 'nothing':
                sent_message = await message.channel.send(f"I think you intent is {classified_intent}")
                await sent_message.add_reaction("✅")
                await sent_message.add_reaction("❌")

                def check(reaction, user):
                    return user == message.author and str(reaction.emoji) in ["✅", "❌"]

                try:
                    reaction, user = await self.wait_for('reaction_add', timeout=1200.0, check=check)
                    if str(reaction.emoji) == "✅":
                        self.add_intent(message.content, classified_intent)
                        await message.channel.send("Intent added to the dictionary.")
                    elif str(reaction.emoji) == "❌":
                        await sent_message.delete()
                        print("wrong intent detection, correction training...")
                        self.add_intent(message.content, "nothing")
                    else:
                        print("Intent not added.")
                except asyncio.TimeoutError:
                    print("You didn't react in time, the intent will not be added and msg deleted.")
                    await sent_message.delete()
            else:
                print("I'm sorry, I didn't understand your message.")

    async def handle_command(self, command, message):
        if command == 'add_intent':
            await message.channel.send("Please provide the phrase and the intent separated by a comma.")
        else:
            pass  # other commands handling

    def add_intent(self, phrase, intent):
        self.intents_db.add_phrase(phrase, intent)
        with self.training_lock:
            if self.training_queue.full():
                self.training_queue.get()
                self.training_queue.task_done()
            self.training_queue.put(self.nltk_model.train)
            if not self.current_training_thread or not self.current_training_thread.is_alive():
                self.current_training_thread = threading.Thread(target=self.run_training)
                self.current_training_thread.start()

    def run_training(self):
        while True:
            next_train_func = self.training_queue.get()
            next_train_func()
            self.training_queue.task_done()
            if self.training_queue.empty():
                break


async def retrieve_message_thread(message: Message) -> List[Dict[str, str]]:
    list_of_messages: List[Message] = []
    # Fetch each message in the thread
    while message.reference:
        message = await message.channel.fetch_message(message.reference.message_id)
        list_of_messages.append(message)

    # Prepare the list of messages with roles
    list_of_messages_dict = [{
        "role": "assistant" if msg.author.bot else "user",
        "content": msg.content
    } for msg in reversed(list_of_messages)]

    return list_of_messages_dict


class OpenAIDiscordBot(discord.Client):
    def __init__(self):
        super().__init__(intents=intents)
        self.conn = sqlite3.connect('mentions.db')
        self.cursor = self.conn.cursor()
        self.cursor.execute(
            '''CREATE TABLE IF NOT EXISTS mentions (
                user_id INTEGER PRIMARY KEY,
                mentions_count INTEGER
            )'''
        )
        self.bot_ignition = True
        self.conn.commit()
        self.brain = OpenAIModel()
        self.mentions_daily_reset()
        self.bot_ignition = False

    async def on_ready(self):
        print(f'{self.user} is connected to the following guilds:')
        for guild in self.guilds:
            print(f'{guild.name}(id: {guild.id})')

    async def on_message(self, message: Message):
        if message.author == self.user:
            return

        # If the user replied to a message from the bot
        if message.reference:
            reference_message = await message.channel.fetch_message(message.reference.message_id)
            if reference_message.author == self.user:
                await self.answer_user_with_context(message)

        if message.content.startswith(f'{self.user.mention}'):
            user_id = message.author.id
            user_mentions = self.get_user_mentions_db(user_id)

            if user_mentions:
                mentions_count = user_mentions[1]
                if mentions_count < 27 or user_id == 140033402681163776:
                    mentions_count += 1
                    self.increment_mentions_db_for_user(user_id, mentions_count)
                    msg = await message.reply("Je réfléchis, laissez-moi un peu de temps...")
                    response = self.brain.get_answer(clean_content(message.content))
                    await msg.edit(content=response)
                else:
                    await message.channel.send('Vous avez atteint le nombre maximum de 27 questions journalières.')
            else:
                self.init_mentions_db_for_user(user_id)
                await self.answer_user(message)
        else:
            if message.guild is not None and len(message.content) > 1:
                print('analysing ' + message.content)
                member = message.guild.get_member(message.author.id)
                if member:
                    print("member: " + str(member.roles))
                else:
                    print("Member none")
                    return
                if not member.roles or len(member.roles) == 0 or (
                        len(member.roles) == 1 and member.roles[0].name == '@everyone'):
                    if self.message_needs_answer(message.content):
                        await self.answer_user(message, without_premessage=True)

    def get_user_mentions_db(self, user_id):
        self.cursor.execute(f'SELECT * FROM mentions WHERE user_id={user_id}')
        user_mentions = self.cursor.fetchone()
        return user_mentions

    def increment_mentions_db_for_user(self, user_id, mentions_count: int):
        self.cursor.execute(f'UPDATE mentions SET mentions_count={mentions_count} WHERE user_id={user_id}')
        self.conn.commit()

    def init_mentions_db_for_user(self, user_id):
        self.cursor.execute(f'INSERT INTO mentions (user_id, mentions_count) VALUES({user_id}, 1)')
        self.conn.commit()

    async def answer_user(self, message: Message, without_premessage=False):
        if not without_premessage:
            msg = await message.reply("Je réfléchis, laissez-moi un peu de temps...")
            response = self.brain.get_answer(message.content)
            await msg.edit(content=response)
        else:
            response = self.brain.get_answer(message.content)
            await message.reply(response)

    def mentions_daily_reset(self):
        now = datetime.datetime.now()
        next_day = now.replace(hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1)
        delta_t = next_day - now
        secs = delta_t.total_seconds()
        threading.Timer(secs, self.mentions_daily_reset).start()
        if not self.bot_ignition:
            conn = sqlite3.connect('mentions.db')
            cursor = conn.cursor()
            cursor.execute("UPDATE mentions SET mentions_count = 0")
            conn.commit()
            conn.close()
            print("mentions count reset success")
        else:
            print("did not reset mentions count cause bot is initializing")

    def message_needs_answer(self, clean_content) -> bool:
        classification = self.brain.classify_message(clean_content)
        none_classification = ('rien de spécial', 'rien de special', 'autres', 'autre',
                               "avis sur une traduction fr d'un autre jeu que danganronpa", "remerciement")
        print('message: ' + clean_content + ' was analysed [' + classification + ']')

        if classification.lower().strip() in none_classification:
            return False
        else:
            return True

    async def answer_user_with_context(self, message):
        message_thread = await retrieve_message_thread(message)
        response = self.brain.get_answer_with_context(message_thread, message.content)
        await message.reply(response)
