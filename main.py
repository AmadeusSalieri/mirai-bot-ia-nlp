import os
from DiscordBot import DiscordBot, OpenAIDiscordBot
from IntentsDB import IntentsDB
from NLTKModel import NLTKModel

BOT_TOKEN = os.environ.get('BOT_TOKEN')
OPENAI_BOT_TOKEN_DEFOU = os.environ.get('OPENAI_BOT_TOKEN_DEFOU')


def mirai_bot_nlp():
    intents_db = IntentsDB()
    nltk_model = NLTKModel(intents_db)
    bot = DiscordBot(intents_db, nltk_model)
    bot.run(BOT_TOKEN)


def mirai_bot_nlp_openai():
    bot = OpenAIDiscordBot()
    bot.run(OPENAI_BOT_TOKEN_DEFOU)


mirai_bot_nlp_openai()
