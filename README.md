# NLU-Discord-Bot

Ce projet a pour objectif de créer un bot discord capable de détecter les intentions d'un utilisateur à partir de sa phrase et de lui répondre en conséquence. Ce bot utilise des techniques de NLP (Natural Language Processing) pour détecter les intentions. Il utilise également l'API d'OpenAI pour améliorer son analyse de phrase.

## Prérequis
Pour utiliser ce projet, vous devez avoir les dépendances suivantes installées :

- discord.py
- openai
- nltk

Vous devez également avoir une clé API valide pour utiliser l'API d'OpenAI.

## Structure du code

Ce projet est divisé en plusieurs parties :

### classes/DiscordBot.py
Cette classe est le coeur du projet. Elle gère la connexion avec le serveur Discord et écoute les messages envoyés par les utilisateurs. Elle utilise la classe NLTKModel pour détecter les intentions des utilisateurs et générer des réponses. Elle gère également les mentions journalières des utilisateurs grâce à une base de données SQLite.

### classes/NLTKModel.py
Cette classe utilise les techniques de NLP pour détecter les intentions des utilisateurs à partir de leurs phrases. Elle utilise également l'API d'OpenAI pour améliorer son analyse de phrase. Elle peut être entraînée avec des exemples d'intentions et de phrases.

### classes/OpenAIModel.py
Cette classe utilise le merveilleux modèle GPT-3 d'OpenAI :3



### main.py
Le fichier principal du projet. Il instancie les différentes classes et lance le bot.

# Utilisation

Pour utiliser ce bot discord, vous devrez d'abord avoir installé Python 3.8 ou une version ultérieure. Il vous faudra également installer les bibliothèques discord.py et sqlite3 en utilisant la commande pip install discord.py sqlite3.

Une fois ces dépendances installées, vous pouvez lancer le bot en éxécutant le fichier main.py. Assurez-vous d'avoir remplacé TOKEN dans le fichier main.py avec le token de votre bot Discord.

# Fonctionnement
Ce bot est conçu pour fonctionner sur un serveur Discord. Il réagit aux mentions faites à son nom. Chaque membre du serveur a droit de mentionner le bot au maximum 10 fois par jour.

La gestion des mentions journalières se fait grâce à une base de données SQLite. A chaque fois qu'un utilisateur mentionne le bot, son nombre de mentions est incrémenté dans la base de données. Si un utilisateur atteint le nombre maximum de mentions autorisées, le bot ne réagira plus à ses mentions.

A chaque démarrage du bot, ou à minuit, un script est lancé pour remettre à zéro les mentions journalières de tous les utilisateurs.

