from typing import Optional

import nltk
from nltk import word_tokenize, NaiveBayesClassifier

# Prerequisites
nltk.download('punkt')


class NLTKModel:
    def __init__(self, intents_db):
        self.intentsDB = intents_db
        self.classifier: Optional[NaiveBayesClassifier] = None
        self.training_count = 0
        self.train()

    def is_classifier_ready(self) -> bool:
        return self.classifier is not None

    @staticmethod
    def extract_features(sentence):
        features = {}
        for word in word_tokenize(sentence):
            features[word] = True
        return features

    def train(self):
        self.training_count += 1
        print(str(self.training_count) + ' - training model...')

        # Training data
        training_data = []
        for intent, questions in self.intentsDB.intents.items():
            for question in questions:
                training_data.append((NLTKModel.extract_features(question), intent))

        self.classifier = NaiveBayesClassifier.train(training_data)

        print(str(self.training_count) + ' - training done !')

    # Classifying method
    def detect_intent(self, sentence):
        if not self.is_classifier_ready():
            print("Classifier is not ready, can't predict intent")
            return None

        features = NLTKModel.extract_features(sentence)
        prob_dist = self.classifier.prob_classify(features)
        samples = prob_dist.samples()
        print('prediction:')
        for label in samples:
            print(prob_dist.prob(label))
        max_prob = max(prob_dist.prob(label) for label in samples)
        if max_prob < 0.8:
            return None
        return prob_dist.max()
