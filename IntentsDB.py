import json

from NLTKModel import NLTKModel


class IntentsDB:
    def __init__(self):
        print('init Intents DB')
        self.intents = {}

        self.unclassified_message = {}

        # Get training data
        with open('./trainingdataresults.json', 'r', encoding='utf-8') as f:
            data = json.load(f)

        self.intents.update(data)

        print(self.intents.keys())
        print(len(self.intents['nothing']), "messages d'entrainement")

    def add_phrase(self, phrase, intent):
        if intent in self.intents:
            self.intents[intent].append(phrase)
        else:
            self.intents[intent] = [phrase]
        self.save()

    def save(self):
        with open('trainingdataresults.json', 'w') as f:
            json.dump(self.intents, f)
